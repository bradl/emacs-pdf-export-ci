FROM alpine
RUN apk add emacs texlive-full git ripgrep fd
RUN git clone --bare https://github.com/brad1111/dotfiles.git $HOME/dotfiles
RUN git --git-dir="$HOME/dotfiles" --work-tree="$HOME" checkout
RUN $HOME/.config/emacs/bin/doom sync
